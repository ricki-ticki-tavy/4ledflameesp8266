function saveSystemSettings() {
    var wiFiSsId = document.getElementById("wiFiSSID").value;
    var wiFiPassword = document.getElementById("wiFiPassword").value;
    var wiFiHostName = document.getElementById("hostName").value;
    var localIP = document.getElementById("localIP").value;
    var wiFiMode = document.getElementById("wifiMode").value;

    $.ajax({
        url: "/systemSettingsApi",
        data: "wiFiSsId=" + wiFiSsId + "&wiFiPassword=" + wiFiPassword + "&wiFiMode=" + wiFiMode + "&wiFiHostName=" + wiFiHostName + "&localIP=" + localIP,
        success: function (response) {
            window.location = "/";
        }.bind(this),
        error: function (error) {
            alert(error.statusText);
        }.bind(this),
        dataType: "json"
    });

}

function saveSpecialSettings() {
    var flameMaxStep = document.getElementById("FLAME_MAX_STEPS").value;
    var flameMinStep = document.getElementById("FLAME_MIN_STEPS").value;
    var flameEffectProbability = document.getElementById("FLAME_EFFECT_PROBABILITY").value;

    sendFlameRequest("paramName=FLAME_MAX_STEPS&value=" + flameMaxStep
        , function () {
            sendFlameRequest("paramName=FLAME_MIN_STEPS&value=" + flameMinStep
                , function () {
                    sendFlameRequest("paramName=FLAME_EFFECT_PROBABILITY&value=" + flameEffectProbability
                    , function(){
                            sendFlameRequest("paramName=saveSettings&value=false",
                                function(){
                                    window.location='/';
                                });

                        });
                });
        });
}

function saveMainParams(){
    sendFlameRequest("paramName=saveSettings&value=false", function(){
        alert("Успешно сохранено");
    });
}

function sendFlameRequest(params, callback) {
    $.ajax({
        url: "/flameApi",
        data: params,
        success: function (response) {
            if (callback) {
                callback();
            }
        }.bind(this),
        error: function (error) {
            alert(error.statusText);
        }.bind(this),
        dataType: "json"
    });
}