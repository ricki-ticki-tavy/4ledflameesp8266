//
// Created by dsporykhin on 19.04.20.
//

#ifndef EFLAMEESP8266_WEBSERVERCONTROLLER_H
#define EFLAMEESP8266_WEBSERVERCONTROLLER_H

#include "../lib/ESPAsyncWebServer/ESPAsyncWebServer.h"
#include "SettingsController.h"

class WebServerController {
private:
    AsyncWebServer* webServer;
public:
    static SettingsController* webSettingsController;
    WebServerController(SettingsController* settingsController);

    static String systemSettingsProcessor(const String& var);

    static void flameApiProcessor(AsyncWebServerRequest *request);
    static void systemSettingsApiProcessor(AsyncWebServerRequest *request);


    static void loadFileByUrl(AsyncWebServerRequest *request);
};

#endif //EFLAMEESP8266_WEBSERVERCONTROLLER_H
