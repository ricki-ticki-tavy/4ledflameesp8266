//
// Created by dsporykhin on 18.04.20.
//

#include "SettingsController.h"
#include <EEPROM.h>
#include <HardwareSerial.h>
#include <Arduino.h>
#include "Defines.h"

SettingsController::SettingsController(bool alarmButton) {
    EEPROM.begin(512);
#if (defined(CON_DEBUG) && (CON_DEBUG >= CON_DEBUG_LEVEL_SYS))
    Serial.println("\r\n Reading ettings...");
#endif
    readSettings(&settings);
    if ((settings.initMarker[0] != SETTINGS_MARKER_0)
        || (settings.initMarker[1] != SETTINGS_MARKER_1)
        || (settings.initMarker[2] != SETTINGS_MARKER_2)
        || (settings.initMarker[3] != SETTINGS_MARKER_3)) {
        // настройки не инициализированы
        settings.initMarker[0] = SETTINGS_MARKER_0;
        settings.initMarker[1] = SETTINGS_MARKER_1;
        settings.initMarker[2] = SETTINGS_MARKER_2;
        settings.initMarker[3] = SETTINGS_MARKER_3;

        settings.version = CURRENT_SETTINGS_VERSION;

        resetWiFi();

        settings.flameLoopInteravlMs = 10;
        settings.flameBrightMax = MAX_PWM;
        settings.flameNormalBright = MAX_FLAME_NORMAL_BRIGHT;
        settings.flameSpeedChangeBrightMin = 4;
        settings.flameSpeedChangeBrightMax = 10;
        settings.flameNewEffectProbabilityPer100Cycles = 100;

        saveSetting(true);
#if (defined(CON_DEBUG) && (CON_DEBUG >= CON_DEBUG_LEVEL_SYS))
        Serial.println(" Settings has never been initialized");
#endif
    } else {

#if (defined(CON_DEBUG) && (CON_DEBUG >= CON_DEBUG_LEVEL_SYS))
        Serial.println(" Settings loaded. Version " + String(settings.version));
#endif
        if (settings.version != CURRENT_SETTINGS_VERSION) {
            if (settings.version == 1) {
                // Init newer values
            }

            if (settings.version <= 2) {
                // Init newer values
            }

            settings.version = CURRENT_SETTINGS_VERSION;
            saveSetting(false);
        }

        if (alarmButton) {

            resetWiFi();
            saveSetting(false);
        }
    }
}
//--------------------------------------------------------------------

void SettingsController::resetWiFi() {
    settings.wifiMode = 1;
    String temp = WIFI_DEFAULT_SID;
    temp.toCharArray(&settings.wifiName[0], sizeof(settings.wifiName));

    temp = WIFI_DEFAULT_PASSWORD;
    temp.toCharArray(&settings.wifiPassword[0], sizeof(settings.wifiPassword));

    temp = WIFI_DEFAULT_HOST_NAME;
    temp.toCharArray(&settings.hostName[0], sizeof(settings.hostName));

    settings.ipAddress[0] = 192;
    settings.ipAddress[1] = 168;
    settings.ipAddress[2] = 0;
    settings.ipAddress[3] = 254;

}
//--------------------------------------------------------------------

void SettingsController::readSettings(Settings *settings) {
    char *bufPtr;
    bufPtr = (char *) settings;
    for (char i = 0; i < sizeof(Settings); i++) {
        bufPtr[i] = EEPROM.read(i);
    }
}
//--------------------------------------------------------------------

void SettingsController::saveSetting(bool restart) {
#if (defined(CON_DEBUG) && (CON_DEBUG >= CON_DEBUG_LEVEL_SYS))
    Serial.print(" Saving settings ...");
#endif

    char *dataPtr = (char *) &settings;
    for (int addr = 0; addr < sizeof(Settings); addr++) {
        EEPROM.write(addr, dataPtr[addr]);
    }
    EEPROM.commit();
#if (defined(CON_DEBUG) && (CON_DEBUG >= CON_DEBUG_LEVEL_SYS))
    Serial.println(" saved\r\n");

#endif

    delay(20);

    if (restart) {
#if (defined(CON_DEBUG) && (CON_DEBUG >= CON_DEBUG_LEVEL_SYS))
        Serial.println("    RESTARTING...\r\n");

#endif
        ESP.restart();
    }
}

//--------------------------------------------------------------------

