//
// Created by dsporykhin on 29.02.20.
// randomly changes normal bright
//

#ifndef EFLAME328_AVGBRIGHTCHANGEEFFECT_H
#define EFLAME328_AVGBRIGHTCHANGEEFFECT_H


#include "AbstractFlameEffect.h"

class AvgBrightChangeFlameEffect: public AbstractFlameEffect {
public:
    AvgBrightChangeFlameEffect(EFlame* eflame);

    void applyEffect();

};


#endif //EFLAME328_AVGBRIGHTCHANGEEFFECT_H
