//
// Created by dsporykhin on 19.04.20.
//

#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include "WiFiController.h"
#include "Defines.h"


WiFiController::WiFiController(SettingsController *settingsController) {
    this->settingsController = settingsController;
    init();
}

void WiFiController::init() {

    IPAddress ipAddress = IPAddress(settingsController->settings.ipAddress[0],
                                    settingsController->settings.ipAddress[1],
                                    settingsController->settings.ipAddress[2],
                                    settingsController->settings.ipAddress[3]);

#if (defined(CON_DEBUG) && (CON_DEBUG >= CON_DEBUG_LEVEL_SYS))
    Serial.println("WiFi        SID: " + String(&settingsController->settings.wifiName[0]));
    Serial.println("       password: " + String(&settingsController->settings.wifiPassword[0]));
    Serial.println("       hostName: " + String(settingsController->settings.hostName));
    Serial.println("       local Ip: " + ipAddress.toString());
    Serial.println("  Current state: " + String(WiFi.getMode()));
    Serial.println("      wifi mode: " + String(settingsController->settings.wifiMode));
    Serial.println("  is persistent: " + String(WiFi.getPersistent()));
#endif

    if ((settingsController->settings.wifiMode != WIFI_OFF) &&
        ((WiFi.hostname() != String(settingsController->settings.hostName))
         || (WiFi.localIP() != ipAddress)
         || (WiFi.gatewayIP() != ipAddress)
         || (settingsController->settings.wifiMode != WiFi.getMode())
         || (WiFi.softAPSSID() != settingsController->settings.wifiName)
         || (WiFi.softAPPSK() != String(settingsController->settings.wifiPassword)))
            ) {

#if (defined(CON_DEBUG) && (CON_DEBUG >= CON_DEBUG_LEVEL_SYS))
        Serial.println("Configuring WiFi...");
#endif

        WiFi.hostname(settingsController->settings.hostName);
        wifi_station_set_hostname(settingsController->settings.hostName);
        WiFi.mode(settingsController->settings.wifiMode == 1 ? WIFI_AP : WIFI_STA);
//        WiFi.forceSleepWake();
        delay(2);
        if (settingsController->settings.wifiMode == 1) {
            WiFi.softAP(settingsController->settings.wifiName, settingsController->settings.wifiPassword);
            delay(10);

            WiFi.softAPConfig(ipAddress, ipAddress, IPAddress(255, 255, 255, 0));

        } else {
            WiFi.begin(settingsController->settings.wifiName, settingsController->settings.wifiPassword);
        }

        WiFi.hostname(settingsController->settings.hostName);
        wifi_station_set_hostname(settingsController->settings.hostName);

        MDNS.begin(String(&settingsController->settings.hostName[0]));
        MDNS.addService("http", "tcp", 80);

#if (defined(CON_DEBUG) && (CON_DEBUG >= CON_DEBUG_LEVEL_SYS))
        Serial.println("   Starting web server...");
#endif
        serverController = new WebServerController(settingsController);

    } else if (settingsController->settings.wifiMode == WIFI_OFF && WiFi.getMode() != WIFI_OFF) {

#if (defined(CON_DEBUG) && (CON_DEBUG >= CON_DEBUG_LEVEL_SYS))
        Serial.println("   Turning WiFi off...");
        WiFi.mode(WIFI_OFF);
        delay(1);
        WiFi.forceSleepBegin();
        delay(1);
#endif

    }
}