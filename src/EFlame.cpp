//
// Created by dsporykhin on 29.02.20.
//

#include "EFlame.h"
#include "Defines.h"

EFlame::EFlame(SettingsController *settingsController) {

    this->settingsController = settingsController;

    effectCounter = 0;
    stepCountToDo = 0;
    totalEffectProbability = 0;
    probabilityChangeAction = 0.0001;
    normalBright = 190 * 4;

    for (int index = 0; index < 4; index++) {
        brightDelta[index] = 0;
        brights[index] = 0;
        pinMode(ledMatrix[index], OUTPUT);
    };

#if (defined(CON_DEBUG) && (CON_DEBUG >= CON_DEBUG_LEVEL_SYS))
    Serial.println("------\r\n"
                           " EFlame starting with params...\r\n"
                           "       loop interval ms: " + String(settingsController->settings.flameLoopInteravlMs) +
                   "\r\n"
                           "             max bright: " + String(settingsController->settings.flameBrightMax) + "\r\n"
                           "          normal bright: " + String(settingsController->settings.flameNormalBright) + "\r\n"
                           "        min change brgt: " +
                   String(settingsController->settings.flameSpeedChangeBrightMin) + "\r\n"
                           "        max change brgt: " +
                   String(settingsController->settings.flameSpeedChangeBrightMax) + "\r\n"
                           "     effect probability: " +
                   String(settingsController->settings.flameNewEffectProbabilityPer100Cycles) + "\r\n"
                           "--------------------------");
#endif


}
//---------------------------------------------------------------------------

double EFlame::doubleRandom() {
    long maxVal = 1200000000L;
    long val = random(maxVal);
    double d = val;
    return d / (double) (maxVal);
}
//---------------------------------------------------------------------------

void EFlame::appendEffect(AbstractFlameEffect *effect) {
    if (effectCounter < MAX_EFFECTS_COUNT) {
        effects[effectCounter++] = effect;
        // суммарная вероятность возникновения нового эффекта
        totalEffectProbability += effect->probability;;
    }
}
//---------------------------------------------------------------------------

double EFlame::doubleAbs(double src) {
    return src < 0 ? -src : src;
}
//---------------------------------------------------------------------------

void EFlame::hdSetLedBright(int index) {
    double coef = settingsController->settings.flameNormalBright / (double) MAX_FLAME_NORMAL_BRIGHT;
    analogWrite(ledMatrix[index], brights[index] * coef);
}
//---------------------------------------------------------------------------

void EFlame::applyNewEffect() {
    // берем некое ранодомное число, не превышающее суммарной вероянтности выпадения всех эффектов.
    double effectSelector = doubleRandom() * totalEffectProbability;
    int newEffectIndex = -1;

    // Все вероятности сложены, как бы, в единый столбик. Выпавшее число не юольше размера этого столбика. ищем в кого
    // в этом столбике попало наше число. Чем выше было значение вероятности для эффекта, тем с большей вероятностью
    // число попадет именно в его диапазон
    for (int index = 0; index < effectCounter; index++) {
        if (effects[index]->probability > effectSelector) {
            newEffectIndex = index;
            break;
        } else {
            effectSelector -= effects[index]->probability;
        }
    }

    if (newEffectIndex != -1) {
        // все нормально. Иначе и быть не должно, но проверка должна быть
        effects[newEffectIndex]->apply();
    }
}
//---------------------------------------------------------------------------

void EFlame::loop() {
#if (defined(CON_DEBUG) && (CON_DEBUG > CON_DEBUG_LEVEL_SYS))
    Serial.print(" loop scnt=" + String(stepCountToDo));
#endif
    if (stepCountToDo <= 0 || doubleRandom() <= probabilityChangeAction) {
        // меняем направление свечения и все такое
        if (stepCountToDo > 0    // именно так! если он больше, но мы тут, то сработала безусловная смена эффекта
            || doubleRandom() <= totalEffectProbability
                                 * settingsController->settings.flameNewEffectProbabilityPer100Cycles
                                 / 100) {
            applyNewEffect();
        }
    } else {
        for (int index = 0; index < 4; index++) {
#if (defined(CON_DEBUG) && (CON_DEBUG > CON_DEBUG_LEVEL_SYS))
            Serial.print("   idx=" + String(index) + ", "
                         + "  Led" + String(index) + "=" + String(brights[index]) + "+" + String(brightDelta[index]) + "->");
#endif
            brights[index] += brightDelta[index];
            if (brights[index] < 0) {
                brights[index] = 0;
            } else if (brights[index] > MAX_PWM) {
                brights[index] = MAX_PWM;
            }
#if (defined(CON_DEBUG) && (CON_DEBUG > CON_DEBUG_LEVEL_SYS))
            Serial.print(String(brights[index]));
#endif
            hdSetLedBright(index);
        }
        stepCountToDo--;
    }
#if (defined(CON_DEBUG) && (CON_DEBUG > CON_DEBUG_LEVEL_SYS))
    Serial.println(".");
#endif
}
//---------------------------------------------------------------------------

SettingsController *EFlame::getSettingsController() {
    return settingsController;
}

//---------------------------------------------------------------------------
