//
// Created by dsporykhin on 29.02.20.
//

#include "HotAgeFlameEffect.h"
#include "Defines.h"

HotAgeFlameEffect::HotAgeFlameEffect(EFlame *eflame) : AbstractFlameEffect(0.1, eflame, true) {

}

void HotAgeFlameEffect::applyEffect() {
    int index = random(0, 3);
//    int bright = random(10, 20) + flame->brightMax - 30;
    int bright = flame->getSettingsController()->settings.flameBrightMax;
    int speed = flame->doubleRandom() * (flame->getSettingsController()->settings.flameSpeedChangeBrightMax
                                         - flame->getSettingsController()->settings.flameSpeedChangeBrightMin - 2)
                + flame->getSettingsController()->settings.flameSpeedChangeBrightMin + 2;
    double brightDelta = bright - flame->brights[index];
    flame->stepCountToDo = round(brightDelta / (double) speed);
    if (flame->stepCountToDo == 0) {
        flame->stepCountToDo = 1;
    }
    flame->brightDelta[index] = brightDelta / (double) flame->stepCountToDo;
#if (defined(CON_DEBUG) && (CON_DEBUG > CON_DEBUG_LEVEL_SYS))
    Serial.println("HotAgeFlameEffect  index=" + String(index) + " brgt=" + String(bright) + "   speed=" + String(speed) + "  totalDelta=" + String(brightDelta));
#endif
}