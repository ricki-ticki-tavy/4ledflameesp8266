//
// Created by dsporykhin on 19.04.20.
//

#include "WebServerController.h"
#include "Defines.h"

SettingsController* WebServerController::webSettingsController;

WebServerController::WebServerController(SettingsController *settingsController_) {
    WebServerController::webSettingsController = settingsController_;

#if (defined(CON_DEBUG) && (CON_DEBUG >= CON_DEBUG_LEVEL_SYS))
    Serial.println(" Starting web server...");
#endif

    webServer = new AsyncWebServer(80);

    webServer->on("/", HTTP_GET, [](AsyncWebServerRequest *request) {
        request->send(SPIFFS, "/index.html", String(), false, systemSettingsProcessor);
    });

    webServer->on("/wifiSettings.html", HTTP_GET, [](AsyncWebServerRequest *request) {
        request->send(SPIFFS, "/wifiSettings.html", String(), false, systemSettingsProcessor);
    });

    webServer->on("/specialSettings.html", HTTP_GET, [](AsyncWebServerRequest *request) {
        request->send(SPIFFS, "/specialSettings.html", String(), false, systemSettingsProcessor);
    });

    webServer->on("/flameApi", HTTP_GET, flameApiProcessor);

    webServer->on("/systemSettingsApi", HTTP_GET, systemSettingsApiProcessor);

    webServer->onNotFound(loadFileByUrl);

    webServer->begin();
}


/**
 * Change system settings handler
 * @param request
 */
void WebServerController::systemSettingsApiProcessor(AsyncWebServerRequest *request){
    String wifiSSID = request->arg("wiFiSsId");
    String wifiPassword = request->arg("wiFiPassword");
    String wifiHostName = request->arg("wiFiHostName");
    String localIP = request->arg("localIP");
    String wiFiMode = request->arg("wiFiMode");

    if (!wifiSSID || wifiSSID.isEmpty()){
        request->send(503, "wiFiSsId can't be null", "wiFiSsId can't be null");
    } else if (!wifiPassword || wifiPassword.isEmpty()){
        request->send(503, "wiFiPassword can't be null", "wiFiPassword can't be null");
    } else if (!wifiHostName || wifiHostName.isEmpty()){
        request->send(503, "wifiHostName can't be null", "wifiHostName can't be null");
    } else if (!wiFiMode || wiFiMode.isEmpty() || wiFiMode.toInt() > 2 || wiFiMode.toInt() < 0){
        request->send(503, "invalid wifiMode", "invalid wifiMode");
    } else if (!localIP || localIP.isEmpty()){
        request->send(503, "localIP can't be null", "localIP can't be null");
    } else if (!IPAddress(0, 0, 0, 0).fromString(localIP)){
        request->send(503, "invalid localIP", "invalid localIP");
    } else {
        IPAddress localIPAddress = IPAddress(0, 0, 0, 0);
        localIPAddress.fromString(localIP);

#if (defined(CON_DEBUG) && (CON_DEBUG >= CON_DEBUG_LEVEL_SYS))
        Serial.println(" New WiFi settings:\r\n"
        "        SSID: " + wifiSSID + "\r\n"
        "    password: " + wifiPassword + "\r\n"
        "   host name: " + wifiHostName + "\r\n"
        "    local Ip: " + localIP + "\r\n"
        "   wifi mode: " + String(wiFiMode));
#endif

        webSettingsController->settings.wifiMode = wiFiMode.toInt();
        wifiSSID.toCharArray(&webSettingsController->settings.wifiName[0], sizeof(webSettingsController->settings.wifiName));
        wifiPassword.toCharArray(&webSettingsController->settings.wifiPassword[0], sizeof(webSettingsController->settings.wifiPassword));
        wifiHostName.toCharArray(&webSettingsController->settings.hostName[0], sizeof(webSettingsController->settings.hostName));

        for (int i= 0; i < 4; i++){
            webSettingsController->settings.ipAddress[i] = localIPAddress[i];
        }

        webSettingsController->saveSetting(true);

    }
}
//----------------------------------------------------------------------


void WebServerController::flameApiProcessor(AsyncWebServerRequest *request){

#if (defined(CON_DEBUG) && (CON_DEBUG >= CON_DEBUG_LEVEL_SYS))
    Serial.println(" INFO: flameApi: \"" + request->url() + "\"");
#endif

    String paramName = request->arg("paramName");
    String value = request->arg("value");
    if (!paramName){
        request->send(404, "paramName can't be null", "paramName can't be null");
    } else if (!value){
        request->send(404, "value can't be null", "value can't be null");
    } else {

#if (defined(CON_DEBUG) && (CON_DEBUG >= CON_DEBUG_LEVEL_SYS))
        Serial.println(" INFO: flameApi parameter name= \"" + paramName + "\" value=" + value);
#endif

        if (paramName == "saveSettings"){
            webSettingsController->saveSetting(value == "true");
        } else if (paramName == "flameInterval"){
            // Intermal in msec between flame loop cycles
            int intVal = value.toInt();
            if (intVal < 1) {
                intVal = 1;
            } else if (intVal > 30) {
                intVal = 30;
            }
            webSettingsController->settings.flameLoopInteravlMs = intVal;
        } else if (paramName == "flameNormalBrightness"){
            // avg brightness
            int intVal = value.toInt();
            if (intVal < 0) {
                intVal = 0;
            } else if (intVal > MAX_FLAME_NORMAL_BRIGHT) {
                intVal = MAX_FLAME_NORMAL_BRIGHT;
            }
            webSettingsController->settings.flameNormalBright = intVal;
        } else if (paramName == "FLAME_MAX_STEPS"){
            // avg brightness
            double dVal = value.toDouble();
            if (dVal < 0.2) {
                dVal = 0.2;
            } else if (dVal > 30) {
                dVal = 30;
            }
            webSettingsController->settings.flameSpeedChangeBrightMax = dVal;
        } else if (paramName == "FLAME_MIN_STEPS"){
            // avg brightness
            double dVal = value.toDouble();
            if (dVal < 0.2) {
                dVal = 0.2;
            } else if (dVal > 30) {
                dVal = 30;
            }
            webSettingsController->settings.flameSpeedChangeBrightMin = dVal;
        } else if (paramName == "FLAME_EFFECT_PROBABILITY"){
            // avg brightness
            double dVal = value.toDouble();
            if (dVal < 0.02) {
                dVal = 0.02;
            } else if (dVal > 100) {
                dVal = 100;
            }
            webSettingsController->settings.flameNewEffectProbabilityPer100Cycles = dVal;
        }

        request->send(200, "200", "200");
    }
}
//----------------------------------------------------------------------


void WebServerController::loadFileByUrl(AsyncWebServerRequest *request) {
    String url = request->url();
    String mime;

    File testFile = SPIFFS.open(request->url(), "r");
    if (!testFile) {

#if (defined(CON_DEBUG) && (CON_DEBUG >= CON_DEBUG_LEVEL_SYS))
        Serial.println("ERROR: url not found: \"" + request->url() + "\"");
#endif

        request->send(404, "text/plan", "not found");
    } else {
        testFile.close();

        if (url.endsWith(".html")) {
            mime="text/html";
        } else if (url.endsWith(".css")) {
            mime="text/css";
        } else if (url.endsWith(".js")) {
            mime="text/js";
        } else if (url.endsWith(".jpg")) {
            mime="image/jpeg";
        } else {
            mime = "text/plan";
        }

        request->send(SPIFFS, url, mime);
    }
}
//----------------------------------------------------------------------


String WebServerController::systemSettingsProcessor(const String &var) {
    if (var == "WIFI_SSID"){
        return webSettingsController->settings.wifiName;
    } else if (var == "WIFI_PASSWORD"){
        return webSettingsController->settings.wifiPassword;
    } else if (var == "WIFI_HOSTNAME"){
        return String(webSettingsController->settings.hostName);
    } else if (var == "WIFI_LOCAL_IP"){
        return IPAddress(webSettingsController->settings.ipAddress[0]
                ,webSettingsController->settings.ipAddress[1]
                ,webSettingsController->settings.ipAddress[2]
                ,webSettingsController->settings.ipAddress[3]).toString();
    } else if (var == "WIFI_MODE"){
        return String(webSettingsController->settings.wifiMode);
    }

    else if (var == "FLAME_INTERVAL"){
        return String(MAX_FLAME_INTERVAL + 1 - webSettingsController->settings.flameLoopInteravlMs);
    } else if (var == "MAX_FLAME_INTERVAL"){
        return String(MAX_FLAME_INTERVAL);
    }

    else if (var == "FLAME_MIN_STEPS"){
        return String(webSettingsController->settings.flameSpeedChangeBrightMin);
    } else if (var == "FLAME_MAX_STEPS"){
        return String(webSettingsController->settings.flameSpeedChangeBrightMax);
    }

    else if (var == "FLAME_EFFECT_PROBABILITY"){
        return String(webSettingsController->settings.flameNewEffectProbabilityPer100Cycles);
    }

    else if (var == "FLAME_NORMAL_BRIGHT"){
        return String(webSettingsController->settings.flameNormalBright);
    } else if (var == "MAX_FLAME_NORMAL_BRIGHT"){
        return String(MAX_FLAME_NORMAL_BRIGHT);
    }

     else if (var == "HALL_SENSOR"){
        return String(String(analogRead(A0)));
    }

}
//----------------------------------------------------------------------
