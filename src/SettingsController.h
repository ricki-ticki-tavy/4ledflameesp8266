//
// Created by dsporykhin on 18.04.20.
//
#ifndef EFLAMEESP8266_SETTINGSCONTROLLER_H
#define EFLAMEESP8266_SETTINGSCONTROLLER_H

#define CURRENT_SETTINGS_VERSION 1
#define SETTINGS_MARKER_0 0x31
#define SETTINGS_MARKER_1 0x32
#define SETTINGS_MARKER_2 0x33
#define SETTINGS_MARKER_3 0x38

#define MAX_FLAME_INTERVAL 30
#define MAX_FLAME_NORMAL_BRIGHT 255

#define MAX_PWM 1023

struct Settings {
    /**
     * Признак, что настройки записаны, а не пустое пространство. Маркеом является последовательность 0x31 0x32 0x33 0x35
     */
    char initMarker[4];
    /**
     * Версия настроек
     */
    unsigned char version;

    /**
     * Настройки WiFi
     */
    char wifiName[64];
    char wifiPassword[64];
    unsigned char ipAddress[4];

    /**
     * Имя станции светильника в сети
     */
    char hostName[64];

    /**
     * Признак надо ли включать WiFi.  WiFiMode
     */
    unsigned char wifiMode;

    /**
     * Интервал между циклами обработки пламени
     */
    int flameLoopInteravlMs;

    /**
     * Выключить через СЕКУНД после включения. 0 = не выключать
     */
    int shutdownAfterSec;

    /**
     * Макс яркость пина
     */
    int flameBrightMax;

    /**
     * Нормальная средняя яркость пламени
     */
    int flameNormalBright;

    // Максимальная скорость изменения яркости
    double flameSpeedChangeBrightMax;

    // Минимальная скорость изменения яркости
    double flameSpeedChangeBrightMin;

    // Коэффициент вероятности возникновения нового эффекта. в 100 циклов
    double flameNewEffectProbabilityPer100Cycles;


};


class SettingsController {
private:
    void readSettings(Settings *settings);

public:
    Settings settings;

    SettingsController(bool alarmButton);

    void saveSetting(bool restart = false);

    void resetWiFi();

};

#endif //EFLAMEESP8266_SETTINGSCONTROLLER_H
