//
// Created by dsporykhin on 29.02.20.
//

#ifndef EFLAME328_FLAMEEFFECT_H
#define EFLAME328_FLAMEEFFECT_H


#include <Arduino.h>
#include "EFlame.h"

class EFlame;

class AbstractFlameEffect {
public:
    // Вероятность срабатывания эффекта
    double probability;

    // Отключение дельт предыдущих эффектов
    bool preventPriorEffect;

    //Объект пламени
    EFlame *flame;

    // конструктор
    AbstractFlameEffect(
            double probability, EFlame *flame, bool preventPriorEffect);


    // метод, выполняющий действие по срабатыванию эффекта
    virtual void apply();


    virtual void applyEffect();

};


#endif //EFLAME328_FLAMEEFFECT_H
