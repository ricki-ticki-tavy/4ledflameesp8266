//
// Created by dsporykhin on 29.02.20.
//

#ifndef EFLAME328_EFLAME_H
#define EFLAME328_EFLAME_H


#include "AbstractFlameEffect.h"
#include "SettingsController.h"

// кол-во форм, поддерживаемых менеджером
#define MAX_EFFECTS_COUNT 10

class AbstractFlameEffect;

class EFlame {
private:
    SettingsController* settingsController;
public:
    // видеоэффекты
    AbstractFlameEffect* effects[MAX_EFFECTS_COUNT];

    // кол-во эффектов
    int effectCounter;

    // кол-во циклов до окончания действия эффекта
    int stepCountToDo;

    // Нормальная средняя яркость
    int normalBright;

    // массив дельт для яркостей 4-х светодиодов
    double brightDelta[4];

    // массив текущих яркостей
    double brights[4];

    // Вероятность смены текущего эффекта новым до ео завершения
    double probabilityChangeAction;

    // рандом от 0 до 1
    double doubleRandom();

    // абсолютное значение
    double doubleAbs(double src);

    // Добавление эффекта
    void appendEffect(AbstractFlameEffect* effect);

    // Рабочий цикл программы
    void loop();



    EFlame(SettingsController* settingsController);

    SettingsController* getSettingsController();

private:
    double totalEffectProbability;

    // Установить яркость свеения светодиода
    void hdSetLedBright(int index);

    void applyNewEffect();

    int ledMatrix[4] = {
            4, 5, 14, 16
    };

};


#endif //EFLAME328_EFLAME_H
