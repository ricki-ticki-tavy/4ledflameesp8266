//
// Created by dsporykhin on 19.04.20.
//

#include "SettingsController.h"
#include "WebServerController.h"

#ifndef EFLAMEESP8266_WIFICONTROLLER_H
#define EFLAMEESP8266_WIFICONTROLLER_H


class WiFiController {
private:
    SettingsController* settingsController;
    WebServerController* serverController;

    void init();
public:
    WiFiController(SettingsController* settingsController);
};

extern WiFiController* wiFiController;
#endif //EFLAMEESP8266_WIFICONTROLLER_H
