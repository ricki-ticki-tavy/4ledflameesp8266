//
// Created by dsporykhin on 29.02.20.
//
#include <ArduinoOTA.h>
#include "EFlame.h"
#include "AvgBrightChangeFlameEffect.h"
#include "HotAgeFlameEffect.h"
#include "NormalizeFlameFlameEffect.h"
#include "TrembleFlameEffect.h"
#include "Defines.h"
#include "WiFiController.h"

EFlame *flame;
long lastWork;


SettingsController *settingsController;
WiFiController *wiFiController;

void setup() {
#ifdef CON_DEBUG
    Serial.begin(921600);
    Serial.println("---");
#endif
    SPIFFSConfig cfg;
    cfg.setAutoFormat(false);
    SPIFFS.setConfig(cfg);
    SPIFFS.begin();
    ArduinoOTA.begin(true);

    // Check for alarm (reset) button. Using Hall sensor for button
    int analogVals = 0;
    for (int i = 0; i < 10; i++) {
        analogVals += analogRead(A0);
        delay(1);
    }
    analogVals /= 10;
    bool alarmButton = (analogVals > 855) || (analogVals < 805); // 830 avg

    if (alarmButton){
        pinMode(16, OUTPUT);
        for (int index = 0; index < 10; index++) {
            digitalWrite(16, HIGH);
            delay(30);
            wdt_reset();
            digitalWrite(16, LOW);
            delay(30);
            wdt_reset();
        };

    }

#if (defined(CON_DEBUG) && (CON_DEBUG >= CON_DEBUG_LEVEL_SYS))
    Serial.println(" Alarm value = " + String(analogVals));
    if (alarmButton) {
        Serial.println(" Alarm button pressed");
    }
#endif

    settingsController = new SettingsController(alarmButton);
    wiFiController = new WiFiController(settingsController);

    flame = new EFlame(settingsController);
    AbstractFlameEffect *effect = new AvgBrightChangeFlameEffect(flame);
    flame->appendEffect(effect);
    effect = new HotAgeFlameEffect(flame);
    flame->appendEffect(effect);
    effect = new NormalizeFlameFlameEffect(flame);
    flame->appendEffect(effect);
    effect = new TrembleFlameEffect(flame);
    flame->appendEffect(effect);

    lastWork = millis();

}

void loop() {
    if ((millis() - lastWork) > settingsController->settings.flameLoopInteravlMs) {
        lastWork = millis();
        flame->loop();
    }
    wdt_reset();
    ArduinoOTA.handle();
}