//
// Created by dsporykhin on 01.03.20.
// tremble effect
//

#ifndef EFLAME328_TREMBLEFLAMEEFFECT_H
#define EFLAME328_TREMBLEFLAMEEFFECT_H


#include "AbstractFlameEffect.h"

#define TrembleFlameEffect_MIN_BRIGHT_CHANGE 30

class TrembleFlameEffect: public AbstractFlameEffect {
public:
    TrembleFlameEffect(EFlame* eFlame);

    void applyEffect();

private:
    double matrix[8][4] = {
            {1, 1, -1.9, -1.8} // up
            , {0.5, 1.3, 0.5, -3.5}    // up-right
            , {-1.9, 1.3, 1.3, -1.9} // right
            , {-3.5, 0.5, 1.3, 0.5}      // down-right
            , {-1.9, -1.9, 1, 1} // down
            , {0.5, -3.5, 0.5, 1.3} // down-left
            , {1, -1.9, -1.9, 1} // left
            , {1.3, 0.5, -3.5, 0.5} // up-left
    };
//    double matrix[8][4] = {
//            {0.8, 0.8, -1.4, -1.4} // up
//            , {0.3, 1, 0.3, -2.5}    // up-right
//            , {-1.4, 0.8, 0.8, -1.4} // right
//            , {-2.5, 0.3, 1, 0.3}      // down-right
//            , {-1.4, -1.4, 0.8, 0.8} // down
//            , {0.3, -2.5, 0.3, 1} // down-left
//            , {0.8, -1.4, -1.4, 0.8} // left
//            , {1, 0.3, -2.5, 0.3} // up-left
//    };

};


#endif //EFLAME328_TREMBLEFLAMEEFFECT_H
