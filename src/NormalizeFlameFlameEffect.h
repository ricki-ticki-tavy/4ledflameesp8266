//
// Created by dsporykhin on 01.03.20.
//

#ifndef EFLAME328_NORMALIZEFLAMEFLAMEEFFECT_H
#define EFLAME328_NORMALIZEFLAMEFLAMEEFFECT_H


#include "AbstractFlameEffect.h"

class NormalizeFlameFlameEffect: public AbstractFlameEffect {
public:
    NormalizeFlameFlameEffect(EFlame* eflame);

    void applyEffect();
};


#endif //EFLAME328_NORMALIZEFLAMEFLAMEEFFECT_H
